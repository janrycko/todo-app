var todoData = [];

function changeState(event) {
	var todoId = Number(event.target.value);
    todoData = todoData.map(todoObject => {
      
    if (todoObject.id === todoId) {
    	todoObject.completed = event.target.checked;
    	console.log(todoObject);
    }

    return todoObject;
  });
}

fetch("http://jsonplaceholder.typicode.com/todos",
    { headers: {
        'Access-Control-Allow-Origin:': '*'
    } })
	.then(resp => resp.json())
    .then(data => {
        todoData = data;
        var html = todoData.map(todoObject => {
			return `<div id="todo-${todoObject.id}">
                        <label>
                            <input type="checkbox"
                                   value="${todoObject.id}"
                                   ${todoObject.completed ? ` checked` : ``} />
                            <h2>${todoObject.title}</h2>
                        </label>
                    </div>`;
		});
		document.getElementById('zadanko').innerHTML = '<form>' + html.join('') + '</form>';
        document.querySelectorAll('input').forEach(inputElement => {
            inputElement.addEventListener('change', changeState);
        });
    });